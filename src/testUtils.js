import { render } from '@testing-library/vue';
import router from './router';
import { store } from './store';

export function renderWithRouter(ui) {
  return render(ui, { router });
}

export function renderWithVuex(ui, customStore = {}) {
  return render(ui, { store: { ...store, ...customStore }});
}

export function renderWithRouterAndVuex(ui, customStore = {}) {
  return render(ui, { router, store: { ...store, ...customStore }});
}
