import { fireEvent } from '@testing-library/vue';
import { renderWithRouterAndVuex } from '@/testUtils';
import Register from '../Register';

describe('Register: Component', () => {
  it('Should Component Renders', () => {
    const { queryByPlaceholderText } = renderWithRouterAndVuex(Register, {
      state: {
        account: {
          token: null
        }
      }
    });

    expect(queryByPlaceholderText('Rick')).toBeInTheDocument();
    expect(queryByPlaceholderText('Sanchez')).toBeInTheDocument();
  });

  it('Should Form Validation Displays Error', async () => {
    const { queryByPlaceholderText, getByRole, queryByText } = renderWithRouterAndVuex(Register, {
      state: {
        account: {
          token: null
        }
      }
    });

    const input = queryByPlaceholderText('rick.sanchez@pm.me');

    await fireEvent.input(input, { target: { value: 'rick.sanchez@pm.me' } });

    const button = getByRole('button');

    await fireEvent.click(button);

    expect(queryByText('Login Error !')).toBeInTheDocument();
  });
});
