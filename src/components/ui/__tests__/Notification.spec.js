import { render, fireEvent, screen } from '@testing-library/vue';
import Notification from '../Notification';

describe('Notification: Component', () => {
  it('Should Component Renders', () => {
    render(Notification, {
      props: {
        message: 'Lorem Ipsum'
      }
    });

    expect(screen.getByText('Lorem Ipsum')).toBeInTheDocument();
  });

  it('Should Click Trigger Works', async () => {
    const { getByRole, emitted } = render(Notification, {
      props: {
        message: 'Lorem Ipsum'
      }
    });

    const button = getByRole('button');

    await fireEvent.click(button);

    expect(emitted()).toHaveProperty('click');
  });
});
