import Vue from 'vue';
import Router from 'vue-router';
import { Home, Login, Register, Settings, Pizza } from './components/views';

Vue.use(Router);

const router = {
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings
    },
    {
      path: '/pizza/:id',
      name: 'pizza',
      component: Pizza
    }
  ]
};

export default new Router(router);
