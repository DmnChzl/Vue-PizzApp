import { SET_TOKEN, SET_ACCOUNT, RESET_ACCOUNT } from './constants';
import { fetchLoginAccount, fetchReadAccount, fetchRegisterAccount, fetchLogoutAccount, fetchUpdateAccount, fetchPswdAccount, fetchDeleteAccount } from '@/services/account';

export const actions = {
  setToken({ commit }, token) {
    commit(SET_TOKEN, token);
  },
  setAccount({ commit }, account) {
    commit(SET_ACCOUNT, account);
  },
  resetAccount({ commit }) {
    commit(RESET_ACCOUNT);
  },
  loginAccount({ dispatch }, { login, password }) {
    dispatch('setAccount', { login });

    return fetchLoginAccount({ login, password })
      .then(({ token }) => {
        return dispatch('readAccount', token);
      })
      .catch(error => {
        dispatch('resetAccount');
        throw error;
      });
  },
  readAccount({ dispatch }, token) {
    dispatch('setToken', token);

    return fetchReadAccount(token)
      .then(account => {
        return dispatch('setAccount', account);
      })
      .catch(error => {
        dispatch('resetAccount');
        throw error;
      });
  },
  registerAccount({ dispatch }, { password, ...account }) {
    dispatch('setAccount', account);

    return fetchRegisterAccount({ password, ...account })
      .then(({ token }) => {
        return dispatch('setToken', token);
      })
      .catch(error => {
        dispatch('resetAccount');
        throw error;
      });
  },
  logoutAccount({ dispatch, getters }) {
    const token = getters.getToken;

    return fetchLogoutAccount(token)
      .then(() => dispatch('resetAccount'))
      .catch(() => dispatch('setToken', null));
  },
  updateAccount({ dispatch, getters }, account) {
    const token = getters.getToken;

    return fetchUpdateAccount(token, account)
      .then(({ updatedId }) => {
        if (!updatedId) {
          throw new Error('No ID');
        }

        return dispatch('setAccount', account);
      })
      .catch(error => {
        throw error;
      })
  },
  pswdAccount({ getters }, credentials) {
    const token = getters.getToken;

    return fetchPswdAccount(token, credentials)
      .then(({ updatedId }) => {
        if (!updatedId) {
          throw new Error('No ID');
        }

        return;
      })
      .catch(error => {
        throw error;
      });
  },
  deleteAccount({ dispatch, getters }) {
    const token = getters.getToken;

    return fetchDeleteAccount(token)
      .then(({ deletedId }) => {
        if (!deletedId) {
          throw new Error('No ID');
        }

        return dispatch('resetAccount');
      })
      .catch(error => {
        throw error;
      });
  }
};
