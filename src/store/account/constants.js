export const SET_TOKEN = '[Account] Set Token';
export const SET_ACCOUNT = '[Account] Set Account';
export const RESET_ACCOUNT = '[Account] Reset Account';
