import { SET_PIZZAS, ADD_PIZZA, UP_PIZZA, DEL_PIZZA, RESET_PIZZAS } from './constants';
import { fetchAllPizzas } from '@/services/pizzas';
import { sortByKey } from '@/utils';

export const actions = {
  setPizzas({ commit }, pizzas) {
    commit(SET_PIZZAS, pizzas);
  },
  addPizza({ commit }, pizza) {
    commit(ADD_PIZZA, pizza);
  },
  upPizza({ commit }, pizza) {
    commit(UP_PIZZA, pizza);
  },
  delPizza({ commit }, id) {
    commit(DEL_PIZZA, id);
  },
  resetPizzas({ commit }) {
    commit(RESET_PIZZAS);
  },
  allPizzas({ dispatch, getters }, key) {
    const pizzas = getters.getPizzas;

    if (pizzas.length === 0) {
      fetchAllPizzas().then(allPizzas => {
        const sortedByLabel = allPizzas.sort(sortByKey(key));
    
        dispatch('setPizzas', sortedByLabel);
      });
    }
  }
};
