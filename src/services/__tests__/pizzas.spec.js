import axios from 'axios';

import { fetchAllPizzas } from '../pizzas';

jest.mock('axios');

describe('Pizzas: Service', () => {
  it("Should 'fetchAllPizzas' Returns Response", async () => {
    axios.get.mockResolvedValueOnce({
      data: [
        {
          id: 'ABCDEF123456',
          label: '4 Cheeses',
          items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ]
    });

    const allPizzas = await fetchAllPizzas();

    expect(axios.get).toHaveBeenCalled();

    expect(allPizzas).toHaveLength(1);
  });

  it("Should 'fetchAllPizzas' Returns Empty Response As Error", async () => {
    axios.get.mockRejectedValueOnce({
      message: 'Internal Server Error'
    });

    const allPizzas = await fetchAllPizzas();

    expect(allPizzas).toHaveLength(0);
  });
});
